const grpc = require(`grpc`);
const servicePath = __dirname + `/../../../../master.proto`;
const proto = grpc.load(servicePath);
const defaultTimeout = 500; // 500 ms

class BambuService {
    constructor(dbconfig, timeoutInMs) {
        const connUrl = `${dbconfig.host}:${dbconfig.port}`;
        this.client = new proto.bambuService.BambuService(connUrl, grpc.credentials.createInsecure());
        this.timeoutInMs = timeoutInMs;
        console.log(`Created new connection to [${connUrl}]`);
        if (this.client) {
            console.log(`Successfully connected to the GRPC server[${dbconfig}`);
        } else {
            console.log(`ERROR : Couldn't connect to the GRPC server[${dbconfig}`);
        }
    }

    getPeopleLikeYou(payload, cb) {
        const timeout = new Date().setMilliseconds(new Date().getMilliseconds() + this.timeoutInMs);
        this.client.getPeopleLikeYou(payload, {deadline: timeout},
                                    (err, resp) => parseRPCResponse(err, resp, cb));
    }
}

// Helper functions 

function isJSONParsable(str) {
    let parsed = null;
    try {
        parsed = JSON.parse(str);
    } catch (err) {
        parsed = false;
    }
    return parsed;
};

function parseRPCResponse(err, resp, cb) {
    if (err) cb(err);
    else if (resp.error) {
        cb(new Error(resp.error));
    } else {
        const parsed = isJSONParsable(resp.result);
        if (parsed) cb(null, parsed);
        else cb(null, resp);
    }
}

module.exports = BambuService;