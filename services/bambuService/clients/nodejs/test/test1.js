const BambuService = require(`../client`);

const dbconfig = { host: `localhost`, port : 2211}; // read from config
const timeout = 500; // ms
const bambuService = new BambuService(dbconfig, timeout);

const payload = {
    "name" : "Daniel",
    "age" : "20",
    "latitude" : "70.42",
    "longitude" : "120.2",
    "monthlyIncome" : "2012",
    "experienced" : "true"
}


bambuService.getPeopleLikeYou(payload, (err, resp) => {
    if (err) console.log(`ERROR : $[{err}]`);
    else console.log(`getPeopleLikeYou RESPONSE[${JSON.stringify(resp)}]`);
});

process.on(`uncaughtException`, function (err) {
    console.log({error: {reason: `uncaughtException`, message: err.message.toString(), stack: err.stack}});
});